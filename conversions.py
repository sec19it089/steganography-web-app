import binascii


DELIMETER_VAL = "1111111111111110"

def rgb_to_hex(r, g, b):
    return '#{:02x}{:02x}{:02x}'.format(r, g, b)


def hex_to_rgb(hexcode):
    return tuple(map(ord, hexcode[1:].decode('hex')))


def str_to_bin(message):
    return bin(int(binascii.hexlify(message), 16)).replace("0b", "")


def bin_to_str(binary):
    return binascii.unhexlify('%x' % (int("0b" + binary, 2)))
