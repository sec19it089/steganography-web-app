from flask import render_template, Flask, flash, Response, json
from flask import request
from encode import hiding_in_picture
from decode import retrieving_message

app = Flask(__name__)


@app.route('/')
def home():
    return render_template("home.html")


@app.route('/encode', methods=['POST'])
def encode():
    source_image = request.files.get('source_enc')
    message_inp = request.form.get('message')

    encoded = hiding_in_picture(source_image, message_inp)
    flash(encoded)
    return render_template("home.html")


@app.route('/decode')
def decode():

    source_image = request.files.get('source_dec')
    decoded = retrieving_message(source_image)
    flash(decoded)
    return render_template("home.html")


if __name__ == '__main__':
    app.run(debug=True)
